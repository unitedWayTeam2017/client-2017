import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-forgotpassword-page',
  templateUrl: './forgotpassword-page.component.html',
  styleUrls: ['./forgotpassword-page.component.css']
})
export class ForgotpasswordPageComponent implements OnInit {

  forgotPasswordForm: FormGroup;

  constructor(private location: Location, private formBuilder: FormBuilder) {
    this.forgotPasswordForm = formBuilder.group({
      email: ['']
    })
  }

  ngOnInit() {
  }

  cancelClicked() {
    this.location.back();
  }

}
