

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingComponent } from '../loading/loading.component';
import { Constant } from '../constant';
import { EventService } from '../service/eventService';
import { Event } from '../model/event';
import { UserService } from '../service/userService';


@Component({
    selector: 'app-event-page',
    templateUrl: './event-page.component.html',
    styleUrls: ['./event-page.component.css'],
    providers: [EventService]
})
export class EventPageComponent implements OnInit {

    constructor(private _router: Router, private eventService: EventService) {
    }

    ngOnInit() {
        this.getEvents()
    }

    eventList: Event[]
    selectedEvent: Event = new Event('name', 'description', 'date', 'time', 'street', 'city', 'state', 'zipcode', 100, 200, 0);
    userAttendingSelEvent: boolean = false;

    isAdminUser() {
        return UserService.currentUser.admin == true
    }
    updateSelEvent() {
        var userEmail = UserService.currentUser.email
        // console.log(userEmail);
        // console.log(this.selectedEvent)
        // console.log(this.selectedEvent['volunteers'])
        
        if (this.selectedEvent['volunteers'].indexOf(userEmail) == -1) {
            // console.log('User not in volunteerList');
            this.userAttendingSelEvent = false
        } else {
            // console.log('User is in volunteerList');
            this.userAttendingSelEvent = true
        }
        // console.log('User Attending Sel Event: ' + this.userAttendingSelEvent)
    }

    getEvents() {
        this.eventService
            .getEvents()
            .subscribe((data) => this.handleGetEventsData(data))
    }

    showParticipants(eventId) {

        // console.log('eventID:' + eventId);

        for(var i =0; i< this.eventList.length; i++){
            // console.log(this.eventList[i])
            if( this.eventList[i].id == eventId){
                console.log('Event ID: ' + i)
                console.log('Participants: ' + this.eventList[i].volunteers.toString())
                console.log(this.eventList[i].volunteers.toString());
            }
        }
    }

    handleGetEventsData(data) {
        this.eventList = data
        console.log(this.eventList)
        this.selectedEvent = this.eventList[0]
        this.updateSelEvent()
    }

    attendEvent() {
        var eventIdAndUserEmail = this.selectedEvent.id.toString() + '-' + UserService.currentUser.email
        console.log('Attending Event: ' + eventIdAndUserEmail);

        this.eventService.attendEvent(eventIdAndUserEmail)
            .subscribe((data) => {
                this.eventActionResponse(data);
            })
    }

    unAttendEvent() {
        var eventIdAndUserEmail = this.selectedEvent.id.toString() + '-' + UserService.currentUser.email
        console.log('UnAttending Event: ' + eventIdAndUserEmail);

        this.eventService.unAttendEvent(eventIdAndUserEmail)
            .subscribe((data) => {
                this.eventActionResponse(data);
            })
    }

    eventActionResponse(data){
        // console.log(data);
        this.getEvents();
    }

    deleteEvent() {
        console.log('Attemtping to Delete Event:')
        console.log(this.selectedEvent)

        this.eventService
            .deleteEvent(this.selectedEvent)
            .subscribe((data) => {
                this.handleDeleteEventResponse(data);
            })
    }

    handleDeleteEventResponse(data: any) {
        console.log(data._body + ': Event Deleted.')
        this.getEvents()
    }



}
