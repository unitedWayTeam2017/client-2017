import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChariTreeGraphComponent } from './chari-tree-graph.component';

describe('ChariTreeGraphComponent', () => {
  let component: ChariTreeGraphComponent;
  let fixture: ComponentFixture<ChariTreeGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChariTreeGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChariTreeGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
