import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/userService';
import { User } from'../model/user';

@Component({
  selector: 'app-game-page',
  templateUrl: './game-page.component.html',
  styleUrls: ['./game-page.component.css']
})
export class GamePageComponent implements OnInit {

  private currentUser: User = UserService.currentUser

  private previousLvlPoints: number;
  private level: number = 1;
  private totalPoints: number = 0;
  private nextLvlPoints: number;
  private advocatePoints: number;
  private volunteerPoints: number;
  private givePoints: number;

  private currentAdvocatePoints: number;
  private currentVolunteerPoints: number;
  private currentGivePoints: number;
  private currentTotal: number;
  private percentTotal: number;
  private currentPercentTotal: number;

  constructor() { }

  ngOnInit() {
    this.dummyData();
    this.calculateNextLevel();
    this.calculateTotalPoints();
    this.calculateCurrentPoints();
    this.caluculateLevelComplete();
  }

  dummyData() {
    this.previousLvlPoints = 5400;
    this.advocatePoints = 500 + this.previousLvlPoints;
    this.volunteerPoints = 300 + this.previousLvlPoints;
    this.givePoints = 100 + this.previousLvlPoints;
    this.level = 4;
  }

  //Formula: Points per level = pointsFromPreviousLevel + ((Level# -1)*900)
  calculateNextLevel() {
    this.nextLvlPoints = this.previousLvlPoints + ((this.level - 1) * 900);
  }

  calculateTotalPoints() {
    this.totalPoints = this.previousLvlPoints + this.givePoints + this.volunteerPoints + this.advocatePoints;
  }

  calculateCurrentPoints() {
    this.currentAdvocatePoints = this.advocatePoints - this.previousLvlPoints;
    this.currentVolunteerPoints = this.volunteerPoints - this.previousLvlPoints;
    this.currentGivePoints = this.givePoints - this.previousLvlPoints;
    this.currentTotal = this.currentAdvocatePoints + this.currentVolunteerPoints + this.currentGivePoints;
  }

  caluculateLevelComplete() {
    this.currentPercentTotal = (this.currentTotal)/(this.nextLvlPoints - this.previousLvlPoints);
    this.percentTotal = this.currentPercentTotal * 100;
  }

}
