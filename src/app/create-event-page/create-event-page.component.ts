import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { Event } from '../model/event';
import { EventService } from '../service/eventService';
import { Constant } from '../constant';



@Component({
  selector: 'app-create-event-page',
  templateUrl: './create-event-page.component.html',
  styleUrls: ['./create-event-page.component.css'],
  providers: [EventService]

})
export class CreateEventPageComponent implements OnInit {

  loading: boolean = false;
  message: string;
  submitAttempt: boolean = false;
  createEventForm: FormGroup;

  constructor(private _router: Router, private formBuilder: FormBuilder, private eventService: EventService) {
    this.createEventForm = this.formBuilder.group({
      // name: ['', Validators.compose([Validators.required, Validators.email])]
      name: ['']
      , date: ['']
      , time: ['']
      , street: ['']
      , city: ['']
      , state: ['']
      , zipcode: ['']
      , description: [''] 
      // , latitude: ['', Validators.compose([Validators.required, Validators.minLength(1)])]
      // , longitude: ['', Validators.compose([Validators.required, Validators.minLength(1)])]
      // , categoryId: ['', Validators.compose([Validators.required, Validators.minLength(1)])]

    })
  }

  ngOnInit() { }

  createEvent() {
    console.log('Attempting to create event: ')
    console.log(this.createEventForm.value)

    this.eventService
      .createEvent(this.createEventForm.value)
      .subscribe((data) => {
        this.handleCreateEventResponse(data);
      });
  }

  handleCreateEventResponse(data: any) {
    console.log(data._body + ': Event Created')
    alert('New Event Created!');
  }


}
