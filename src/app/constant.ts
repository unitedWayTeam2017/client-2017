import { User } from "./model/user";


export class Constant {

    static INVALID_LOGIN_MESSAGE = 'Invalid username or password.'

    static EVENT_CREATION_SUCESS = 'Event Creation: Success'

    static ADD_ADMIN_ACCESS = 'ADD'
    static REMOVE_ADMIN_ACCESS = 'REMOVE'

    static currentUser: User

}
