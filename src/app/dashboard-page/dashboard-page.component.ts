import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.css']
})
export class DashboardPageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  loginPage() {
    this.router.navigateByUrl('/login')
  }

  eventPage() {
    this.router.navigateByUrl('/event')
  }

  createEventPage() {
    this.router.navigateByUrl('/create-event')
  }

  adminPage() {
    this.router.navigateByUrl('/admin-manager')
  }

  donatePage() {
    this.router.navigateByUrl('/donate')

  }
}
