import { Component } from '@angular/core';
import { LoginPageComponent } from './login-page/login-page.component';
import { Router } from '@angular/router';
import { UserService } from './service/userService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

constructor(private router: Router){}

  title = 'app';
  userAdminAccess: boolean = UserService.isCurrentUserAdmin()

  dashPage() {
  this.router.navigateByUrl('/dashboard')
  }

  flipAdminAccess(){
    UserService.currentUser.admin = !UserService.currentUser.admin
    this.userAdminAccess = UserService.isCurrentUserAdmin()
    console.log('Admin Access:');
    console.log(UserService.currentUser.admin);
  }

}
