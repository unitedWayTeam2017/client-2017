import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { LoginPageComponent } from './login-page/login-page.component';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { SignupPageComponent } from './signup-page/signup-page.component';
import { ForgotpasswordPageComponent } from './forgotpassword-page/forgotpassword-page.component';
import { EventPageComponent } from './event-page/event-page.component';
import { CreateEventPageComponent } from './create-event-page/create-event-page.component';
import { DonatePageComponent } from './donate-page/donate-page.component';
import { AdminManagerComponent } from './admin-manager/admin-manager.component';
import { AuthGuard } from './routes/auth.guard'
import { AdminGuard } from './routes/admin.guard'
import { UserService } from './service/userService'
import { DonateConfirmationComponent } from './donate-confirmation/donate-confirmation.component'
import { GamePageComponent} from './game-page/game-page.component'


const routes: Routes = [

  { path: "login", component: LoginPageComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: "dash", component: DashboardPageComponent },
  { path: "sign-up", component: SignupPageComponent },
  { path: "forgot-password", component: ForgotpasswordPageComponent },
  { path: "event", component: EventPageComponent, canActivate: [AuthGuard] },
  { path: "create-event", component: CreateEventPageComponent, canActivate: [AdminGuard] },
  { path: "donate", component: DonatePageComponent },
  { path: "payment-confirmation", component:DonateConfirmationComponent},
  { path: "admin-manager", component: AdminManagerComponent, canActivate: [AdminGuard] },
  { path: "game", component: GamePageComponent}

]

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      { useHash: true }
      // { enableTracing: true }
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthGuard,
    AdminGuard,
    UserService
  ]

})

export class AppRoutingModule { }

