import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Login } from '../model/login';
import { User } from '../model/user';
import { Constant } from '../constant';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { FormGroup } from '@angular/forms';


@Injectable()
export class UserService {
    constructor(private http: Http) { }

    private userUrl = 'http://localhost:8080/'
    static currentUser: User = new User('initUser', '', false)
    // currentUser: User = new User( 'email', 'password', 'false')

    static showCurrentUser(){
        console.log("Current User: ")
        console.log(UserService.currentUser);
    }

    static isCurrentUserAdmin(){
        return UserService.currentUser.admin
    }

    getAllUsers() {
        const url = this.userUrl + 'user/'
        return this.http
            .post(url, UserService.currentUser)
            .map((res: Response) => { return res.json() })
            .catch(this.handleError);
    }

    reqLogin(login: Login) {

        var url = this.userUrl + 'login/'
        return this.http
            .post(url, login)
            .map((res: Response) => this.handleLoginData(res))
            .catch(this.handleError);
    }

    setAdminUser(email) {

        var url = this.userUrl + 'setAdminUser?email=' + email

        // console.log('setAdminUser: UserService: Url:')
        // console.log(url)

        return this.http
            .get(url, email)
            .map((res: Response) => { return res })
            .catch(this.handleError)
    }

    removeAdminUser(email) {

        var url = this.userUrl + 'removeAdminUser?email=' + email

        console.log('removeAdminUser: UserService: Url:')
        console.log(url)

        return this.http
            .get(url, email)
            .map((res: Response) => { return res })
            .catch(this.handleError)
    }

    getAdminUsers() {
        var url = this.userUrl + 'getAdminUsers'

        return this.http
            .get(url)
            .map((res: Response) => { return res.json() })
            .catch(this.handleError)
    }

    handleLoginData(res) {
        console.log(res)
        if (res._body != '') {
            return res.json()
        } else {
            return Constant.INVALID_LOGIN_MESSAGE
        }
    }

    handleError(error: any) {
        console.error(error);
        return error
        // return Observable.throw(error.json().error || 'Server error');
    }



}