import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Login } from '../model/login';
import { Constant } from '../constant';
import { Event } from '../model/event';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class EventService {
    constructor(private http: Http) { }

    private baseUrl = 'http://localhost:8080/'

    getEvents() {
        return this.http
            .get(this.baseUrl + 'event')
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    attendEvent(eventIdAndUserEmail){

        var url = this.baseUrl + 'attend-event?eventIdAndUserEmail=' + eventIdAndUserEmail
        // console.log('URL: ' + url);
        
        return this.http
            .get(url)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    unAttendEvent(eventIdAndUserEmail){

        var url = this.baseUrl + 'un-attend-event?eventIdAndUserEmail=' + eventIdAndUserEmail
        // console.log('URL: ' + url);
        
        return this.http
            .get(url)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    createEvent(event: Event) {
        return this.http
            .post(this.baseUrl + 'create-event', event)
            .map((res: Response) => this.handleResponse(res))
            .catch(this.handleError);
    }

    deleteEvent( event: Event ) {        
        return this.http
            .post(this.baseUrl + 'delete-event', event)
            .map((res: Response) => this.handleResponse(res))
            .catch(this.handleError);
    }

    handleResponse(res: any) {
        return res
    }

    handleError(error: any) {
        console.error(error);
        return error
    }



}