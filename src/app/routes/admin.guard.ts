import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../service/userService';

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(private userService: UserService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {

    return Promise.resolve(true)
    
    // console.log('Admin Guard:');
    // console.log(UserService.currentUser);
    
    // const isAdminUser = UserService.currentUser.admin

    // if (isAdminUser) {
    //   return Promise.resolve(true);
    // } else {
    //   console.log(`Admin Access required for this route:  ${state.url}`)
    // //   this.router.navigateByUrl('/login')
    //   return Promise.resolve(false)
    // }
    
  }
}