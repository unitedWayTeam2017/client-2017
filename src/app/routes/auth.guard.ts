import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../service/userService';
import { Constant } from '../constant';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private userService: UserService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {


    return Promise.resolve(true)


    // console.log(UserService.currentUser);
    
    // var isLoggedIn = UserService.currentUser.email != ''


    // if (isLoggedIn) {
    //   return Promise.resolve(true);
    // } else {
    //   console.log(`User does not have access to page:  ${state.url}`)
    //   this.router.navigateByUrl('/login')
    //   return Promise.resolve(false)
    // }

  }
}
