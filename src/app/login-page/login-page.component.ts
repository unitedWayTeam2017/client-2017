import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingComponent } from '../loading/loading.component';
import { UserService } from '../service/userService';
import { Login } from '../model/login';
import { User } from '../model/user';
import { Constant } from '../constant';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],
  providers: [UserService]
})
export class LoginPageComponent implements OnInit {

  loading: boolean = false;
  message: string;
  submitAttempt: boolean = false;
  loginForm: FormGroup;

  constructor(private _router: Router, private userService: UserService, private formBuilder: FormBuilder) {
    this.loginForm = this.formBuilder.group({
      // email: ['', Validators.compose([Validators.required, Validators.email])],
      // password: ['', Validators.compose([Validators.required, Validators.minLength(8)])]

      email: ['' ],
      password: ['']
    })
  }

  ngOnInit() {
  }

  submitClicked() {
    this.submitAttempt = true;
    if (this.loginForm.valid) {
      this.loading = true;
      console.log(this.loginForm.value);
      this.userService
        .reqLogin(this.loginForm.value)
        .subscribe((data) => {
          this.handleLogin(data);
        });
    }
    setTimeout(() => {
      this.loading = false;
    }, 5000);
  }

  handleLogin(data) {
    console.log('User Returned from Server()')
    console.log(data)
    if (data != Constant.INVALID_LOGIN_MESSAGE) {

      const user: User = new User(data.email, '', data.admin)

      UserService.currentUser = user

      // console.log('Current User:')
      // console.log(UserService.currentUser);

      this.loading = false;
      this._router.navigate(['dash']);
    } else {
      this.loading = false
      this.message = Constant.INVALID_LOGIN_MESSAGE
    }

  }

  signUpClicked() {
    console.log("SignUp Clicked")
    this._router.navigate(['sign-up']);
  }

  forgotPasswordClicked() {
    console.log("Forgot Password Clicked");
    this._router.navigate(['forgot-password']);
  }

}
