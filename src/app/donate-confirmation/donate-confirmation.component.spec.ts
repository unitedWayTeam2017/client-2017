import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonateConfirmationComponent } from './donate-confirmation.component';

describe('DonateConfirmationComponent', () => {
  let component: DonateConfirmationComponent;
  let fixture: ComponentFixture<DonateConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonateConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonateConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
