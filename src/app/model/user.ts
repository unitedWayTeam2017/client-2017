export class User {

    email: String;
    password: String;
    admin: boolean;
  
    constructor(email: string, password: string, admin: boolean) {
      this.email = email
      this.password = password
      this.admin = admin
    }
  }
  