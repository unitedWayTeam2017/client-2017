import { User } from './user';

export class Event {

    static totalNumEvents: number

    id :number
    name: string;
    description: string;
    date: string;
    time: string;
    street: string;
    city: string;
    state: string;
    zipcode: string;
    latitude: number;
    longitude: number;
    categoryId: number;
    volunteers: String[] = [];

    public Event() {}

    constructor( name: string, description: string, date: string, time: string, street: string, city: string, state: string, zipcode: string, latitude: number, longitude: number, categoryId: number) {
        this.id = Event.totalNumEvents++;
        this.name = name;
        this.description = description;
        this.date = date;
        this.time = time;
        this.street = street;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.categoryId = categoryId;
    }

  }