import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingComponent } from '../loading/loading.component';
import { UserService } from '../service/userService';
import { User } from '../model/user'
import { Login } from '../model/login';
import { Constant } from '../constant';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-manager',
  templateUrl: './admin-manager.component.html',
  styleUrls: ['./admin-manager.component.css'],
  providers: [UserService]
})
export class AdminManagerComponent implements OnInit {

  allUsers: User[] = [];
  adminForm: FormGroup;
  adminUsers: User[] = [];
  users: User[] = [];
  selectedAdminUser: User = new User('admin', 'pass', false);
  emailInput: string;
  emailInputForm: FormGroup;
  currentUser: any = UserService.currentUser.email

  constructor(private _router: Router, private userService: UserService, private formBuilder: FormBuilder) {
    this.emailInputForm = this.formBuilder.group({
      emailInput: ['']
    })
  }

  ngOnInit() {
    UserService.showCurrentUser()
    this.getAllUsers();
    this.byPassAdminCheck();
  }

  byPassAdminCheck(){
    console.log('*** Dev - Bypass Admin Check to add / remove access! ***');
    UserService.currentUser.admin = true
    
  }

  getAllUsers() {
    this.userService
      .getAllUsers()
      .subscribe((data) => {
        this.allUsers = data
        console.log('model.allUsers:');
        console.log(this.allUsers);
      })
  }

  getAdminUsers() {
    this.adminUsers = []
    this.userService
      .getAdminUsers()
      .subscribe((data) => {
        this.handleSetAdminResponse(data)
      })
  }

  setAdminAccess(email) {
    console.log('setAdminAccess(): Email Param: ' + email);    

    if (UserService.currentUser.admin == true) {
      this.userService
        .setAdminUser(email)
        .subscribe((data) => {
          this.handleRes(data)
        })
    } else {
      console.log('User not able to change Admin Access.');
      UserService.showCurrentUser();
    }
  }

  setAdminAccessByEmailInput(emailInput) {
    console.log('setAdminAccessByEmailInput()');
    this.setAdminAccess(emailInput)
  }

  removeAdminAcess(email) {
    console.log('removeAdminAcess(): param email: ' + email);
    if (UserService.currentUser.admin == true) {
      this.userService
        .removeAdminUser(email)
        .subscribe((data) => {
          this.handleRes(data)
        })
    } else {
      console.log('User not able to change Admin Access.');
      UserService.showCurrentUser();
    }
  }

  handleRes(data) {
    console.log('handleRes():')
    console.log(data._body)
    // this.getAdminUsers()
    this.getAllUsers()
  }

  handleSetAdminResponse(data) {
    console.log('AdminUsers');
    console.log(data)
    for (let i in data) {
      var email = data[i]['email']
      var pass = data[i]['password']
      var admin = data[i]['admin']
      var user = new User(email, pass, admin)

      if (!this.userInArray(user, this.adminUsers)) {
        this.adminUsers.push(user)
      } else {
        // console.log("User already in AdminUsers");
      }
    }
  }

  userInArray(user: User, userArr: User[]) {
    for (let i in userArr) {
      if (user.email == userArr[i].email) {
        return true;
      }
    } return false;
  }

  manageUser(event) {
    console.log('Event ', event.selectedAdminUser);
  }

}

