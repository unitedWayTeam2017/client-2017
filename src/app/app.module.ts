import { BrowserModule } from '@angular/platform-browser';
import { NgModule ,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { LoadingComponent } from './loading/loading.component';
import { SignupPageComponent } from './signup-page/signup-page.component';
import { ForgotpasswordPageComponent } from './forgotpassword-page/forgotpassword-page.component';
import { EventPageComponent } from'./event-page/event-page.component';
import { DonatePageComponent } from'./donate-page/donate-page.component';
import { DonateConfirmationComponent } from './donate-confirmation/donate-confirmation.component'
import { HttpModule } from '@angular/http'; 
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateEventPageComponent } from './create-event-page/create-event-page.component';
import { AdminManagerComponent } from './admin-manager/admin-manager.component'
import { GamePageComponent } from './game-page/game-page.component';
import { ChariTreeGraphComponent } from './chari-tree-graph/chari-tree-graph.component';
import { VolunteerPageComponent } from './volunteer-page/volunteer-page.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    DashboardPageComponent,
    LoadingComponent,
    SignupPageComponent,
    ForgotpasswordPageComponent,
    EventPageComponent,
    CreateEventPageComponent,
    DonatePageComponent,
    DonateConfirmationComponent,
    AdminManagerComponent,
    GamePageComponent,
    ChariTreeGraphComponent,
    VolunteerPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RoundProgressModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
