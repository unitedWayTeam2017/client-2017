import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { NgStyle } from '@angular/common';
import './donate-page-workflow.js';

declare let paypal: any;

@Component({
  selector: 'app-donate-page',
  templateUrl: './donate-page.component.html',
  styleUrls: ['./donate-page.component.css']
})
export class DonatePageComponent implements AfterViewChecked {

  constructor() { }

  title = 'app';
  buttonColor: string ='#000';

  public didPaypalScriptLoad: boolean = false;
  public loading: boolean = true;
  public btnSelected: string = null;

  public paymentAmount: number;

  public onSelectBox(event) {
    this.btnSelected = event.target.id;
    console.log(this.btnSelected);
  }

  public onSelectedAmount(selectedAmount) {
     this.paymentAmount = selectedAmount;
  }

  public paypalConfig: any = {
    env: 'sandbox',
    client: {
      sandbox: 'Adz0mmg9MlxMPBcyydljBDllEu8uSC5ftq7o4pmQetwQQA4ZCWt_2tt-QvOAa0YdUyV-DqBM75LQBmbB',
      production: 'xxxxxxxxxx'
    },
    commit: true,
    payment: (data, actions) => {

      return actions.payment.create({
        payment: {
           transactions: [
            { amount: { total: this.paymentAmount, currency: 'USD' } }
          ]
        },
        experience: {
          input_fields: {
              no_shipping: 1
          }
      }
      });
    },
    onAuthorize: (data, actions) => {
      return actions.payment.get().then(function(paymentDetails) {

        // Show a confirmation using the details from paymentDetails
        // Then listen for a click on your confirm button

        document.querySelector('#confirm-button')
            .addEventListener('click', function() {

            // Execute the payment

            return actions.payment.execute().then(function() {
                // Show a success page to the buyer

            });
          });
      });
    }
  };

  public ngAfterViewChecked(): void {
    if(!this.didPaypalScriptLoad) {
      this.loadPaypalScript().then(() => {
        paypal.Button.render(this.paypalConfig, '#paypal-button');
        this.loading = false;
      });
    }
  }

  public loadPaypalScript(): Promise<any> {
    this.didPaypalScriptLoad = true;
    return new Promise((resolve, reject) => {
      const scriptElement = document.createElement('script');
      scriptElement.src = 'https://www.paypalobjects.com/api/checkout.js';
      scriptElement.onload = resolve;
      document.body.appendChild(scriptElement);
    });
  }

  public toggleSelectedColor() {
   this.buttonColor = '#4286f4';
  }

  ngOnInit() {
  }

}
